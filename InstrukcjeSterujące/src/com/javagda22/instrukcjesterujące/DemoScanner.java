package com.javagda22.instrukcjesterujące;

import java.util.Scanner;

public class DemoScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj swoje imie:");
        String imie = scanner.nextLine();

        System.out.println("Podaj swoje nazwisko:");
        String nazwisko = scanner.nextLine();

        System.out.println("Witaj  " + imie + " " + nazwisko + " !");
        // napisz podobną aplikację, poproś użytkownika o jego imie i nazwisko
        // a następnie przywitaj użytkownika i wypisz jego imie i nazwisko w jednej
        // linii
    }
}
