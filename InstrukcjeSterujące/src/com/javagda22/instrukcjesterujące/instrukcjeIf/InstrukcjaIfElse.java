package com.javagda22.instrukcjesterujące.instrukcjeIf;

public class InstrukcjaIfElse {
    public static void main(String[] args) {
        if (2 > 3) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        if (4 < 5) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        if ((2 - 2) == 0) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        if (true) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        if (9 % 2 == 0) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        if (9 % 3 == 0) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
    }
}
