package com.javagda22.instrukcjesterujące.instrukcjaWhile;

import java.util.Scanner;

public class CzytanieWejscia {
    public static void main(String[] args) {
        
        // wiem że chcę odczytać 20 linii
//        for (int i = 0; i < 20; i++) {
//            String linia = scanner.nextLine();
            //...
//        }

        Scanner scanner = new Scanner(System.in);
        boolean czyPracuje = true;

        while (czyPracuje){
            String linia = scanner.nextLine();

            if(linia.equals("apciu")) {
                System.out.println("Na zdrowie");
            } else if(linia.equals("quit")){
                czyPracuje = false;
            }
        }
        System.out.println("Program zakończył pracę!");



    }
}





