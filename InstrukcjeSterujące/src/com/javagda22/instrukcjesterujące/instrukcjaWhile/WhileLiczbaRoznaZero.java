package com.javagda22.instrukcjesterujące.instrukcjaWhile;

import java.util.Scanner;

public class WhileLiczbaRoznaZero {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean czyPracuje = true;
        while (czyPracuje) {
            int liczba = scanner.nextInt();
            if (liczba > 0) {
                System.out.println("Hello World!");
            } else {
                // ustawienie false spowoduje naturalne zakończenie
                // pętli w następnym obiegu (warunek nie będzie spełniony)
                czyPracuje = false;
                break; // spowoduje przerwanie pętli
            }
        }

        int liczba = scanner.nextInt();
        while ( liczba > 0){
            System.out.println("Hello World!");
            liczba = scanner.nextInt();
        }

    }
}
