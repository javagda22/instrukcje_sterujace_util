package com.javagda22.instrukcjesterujące.instrukcjaWhile;

import java.util.Scanner;

public class WhileDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int iloscPowtorzen = scanner.nextInt();
        // pętla for w porównaniu do pętli while
        for (int licznik = 0; licznik < iloscPowtorzen; licznik++) {
            System.out.println(licznik);
        }

        int licznik = 0; // jeden raz inicjalizacja zmiennej z wartością 0
        while (licznik < iloscPowtorzen) { // sprawdzenie warunku PRZED wykonaniem
            System.out.println(licznik); // wykonanie

            licznik++; // inkrementacja
        }
    }
}
