package com.javagda22.instrukcjesterujące.instrukcjaWhile;

public class CwiczenieWhile {
    public static void main(String[] args) {

        // liczby parzyste z zakresu -100 - 100 w tej samej linii oddzielone średnikami
        int licznik = -100;
        while (licznik < 100) {
            if (licznik % 2 == 0) {
                System.out.print(licznik + ";");
            }
            licznik++;
        }

        //litery od 'a' do 'z'
        char literka = 'a';
        while (literka <= 'z') {
            System.out.println(literka);
            literka++;
        }
    }
}
