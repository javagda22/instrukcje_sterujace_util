package com.javagda22.instrukcjesterujące.tablice;

// slajd 99
public class TabliceCwiczenia {
    public static void main(String[] args) {
        // 1.
        int[] tablica = new int[]{1, 3, 5, 10};

        // 2.
        System.out.println(tablica[1]);
        System.out.println(tablica[3]);

        System.out.println(tablica[1] + " " + tablica[3]);

        // jeśli zaadresujemy indeks który jest poza
        // rozmiarem tablicy, otrzymamy błąd ArrayIndexOutOfBoundsException
//        System.out.println(tablica[5]);

        // tablica.length - rozmiar tablicy
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }

        // wypisanie elementów o parzystym indeksie
        for (int i = 0; i < tablica.length; i++) {
            if (i % 2 == 0) { // 1 i 3 element
                // 0%2==0 , 2%2 ==0
                System.out.println(tablica[i]);
            }
        }

        // wypisanie tylko wartości parzystych
        // i - indeks
        // tablica[i] - wartość
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] % 2 == 0) {
                System.out.println(tablica[i]);
            }
        }

        // wypisanie tablicy w odwrotnej kolejności
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.println(tablica[i]);
        }
    }
}
