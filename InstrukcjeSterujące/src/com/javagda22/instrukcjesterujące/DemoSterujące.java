package com.javagda22.instrukcjesterujące;

public class DemoSterujące {
    public static void main(String[] args) {
        double zmienna = 20.0;

        if(zmienna > 25.0){
            // warunek jest spełniony
            System.out.println(" :) ");
        } else {
            // warunek nie jest spełniony
            System.out.println(" :( ");
        }
    }
}
