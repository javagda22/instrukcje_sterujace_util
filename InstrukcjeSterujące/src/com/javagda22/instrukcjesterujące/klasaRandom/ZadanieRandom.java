package com.javagda22.instrukcjesterujące.klasaRandom;

import java.util.Random;
import java.util.Scanner;

public class ZadanieRandom {
    public static void main(String[] args) {
        Random generator = new Random();

        int losowaLiczba = generator.nextInt(199) + 1;
        System.out.println("Czy " + losowaLiczba + " > 100 ? (tak/nie)");
        Scanner scanner = new Scanner(System.in);

        String linia = scanner.nextLine();

        // kiedy porównujemy dwa String'i powinniśmy stosować metodę "equals"
        // zamiast porównywać referencję do obiektów metoda equals porównuje
        // zawartość zmiennych (czyli tekst zmiennej)

        if (losowaLiczba > 100 && linia.equals("tak")) { // warunek pierwszy
            System.out.println("Zgadza się!");
        } else if (losowaLiczba < 100 && linia.equals("nie")) { // warunek drugi
            System.out.println("Zgadza się!");
        } else { // w przeciwnym razie
            System.out.println("Ta odpowiedź nie jest poprawna!");
        }
    }
}
