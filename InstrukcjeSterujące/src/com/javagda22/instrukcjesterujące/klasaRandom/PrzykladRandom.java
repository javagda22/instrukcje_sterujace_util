package com.javagda22.instrukcjesterujące.klasaRandom;

import java.util.Random;
import java.util.Scanner;

public class PrzykladRandom {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();

        int losowaLiczba = generator.nextInt(199) + 1;
        System.out.println("Losowa liczba ma wartość: " + losowaLiczba);
    }
}
