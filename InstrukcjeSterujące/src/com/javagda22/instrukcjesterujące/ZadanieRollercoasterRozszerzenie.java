package com.javagda22.instrukcjesterujące;

import java.util.Scanner;

public class ZadanieRollercoasterRozszerzenie {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj wagę:");
        // zamiast string, ładujemy int!
        int waga = scanner.nextInt();

        System.out.println("Podaj wzrost:");
        int wzrost = scanner.nextInt();

        System.out.println("Podaj wiek:");
        int wiek = scanner.nextInt();

        boolean warunek_waga = waga < 180;
        boolean warunek_wiek = wiek > 10 && wiek < 80;
        boolean warunek_wzrost = wzrost > 150 && wzrost < 220;

        if (warunek_waga && warunek_wiek && warunek_wzrost) {
            System.out.println("Wszystko ok! Możesz wejść!");
        } else {
            System.out.println("Nie możesz wejść!");
            if (!warunek_waga) { // warunek wagi nie jest spełniony
                System.out.println("Nie możesz wejść, jesteś zbyt ciężki/a");
            }
            if (!warunek_wiek) { // warunek wieku nie jest spełniony
                System.out.println("Nie możesz wejść z powodu wieku!");
            }
            if (!warunek_wzrost) {
                System.out.println("Nie możesz wejść z powodu wzrostu!");
            }
        }
    }
}
