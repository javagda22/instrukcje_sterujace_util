package com.javagda22.instrukcjesterujące;

import java.util.Scanner;

public class ZadanieRollercaster {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj wagę:");
        // zamiast string, ładujemy int!
        int waga = scanner.nextInt();

        System.out.println("Podaj wzrost:");
        int wzrost = scanner.nextInt();

        if (waga < 180 && wzrost > 150) {
            // możemy wejść!
            System.out.println("Wszystko ok, możesz wejść!");
        } else {
            System.out.println("Niestety nie możesz wejść!");
        }
    }
}
