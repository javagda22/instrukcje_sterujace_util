package com.javagda22.instrukcjesterujące.instrukcjaDoWhile;

import java.util.Scanner;

public class DemoDoWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        int liczba = scanner.nextInt();
//        while ( liczba > 0){
//            System.out.println("Hello World!");
//            liczba = scanner.nextInt();
//        }

        int liczba;
        do {
            System.out.println("Hello World!");
            liczba = scanner.nextInt();
        }while (liczba > 0);

    }
}
