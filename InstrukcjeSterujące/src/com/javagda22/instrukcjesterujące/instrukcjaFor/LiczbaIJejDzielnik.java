package com.javagda22.instrukcjesterujące.instrukcjaFor;

import java.util.Scanner;

public class LiczbaIJejDzielnik {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj zakres:");
        int zakres = scanner.nextInt();

        System.out.println("Podaj dzielnik:");
        int dzielnik = scanner.nextInt();

        // dla podanej poniżej pętli iteracja nastąpi dla liczb:
        // 0, 1, 2, 3, ... aż do zakres-1
        for (int licznik = 0; licznik < zakres; licznik++) {
            if (licznik % dzielnik == 0) { // nie ma reszty z dzielenia
                // liczba jest podzielna
                System.out.println("Liczba podzielna:" + licznik);
            }
        }
//        for (int i = 0; i < ; i++) {
//
//        }

        int liczba = 5;

        liczba++; // dodanie do zmiennej liczba wartości 1
        liczba += 2; // dodanie do zmiennej liczba wartości 2;
        liczba += 5; // to jest to samo co :
        // liczba = liczba + 5;

        for (int i = 0; i < zakres; i += 2) { //wypisanie co 2 cyfry
            System.out.println(i);
        }

//        for (char i = 'a'; i < 'z'; i=i+2) { // problem z rzutowaniem
        for (char i = 'a'; i < 'z'; i += 2) {
            System.out.println(i);
        }

        // wypisanie od 'a' do 'z'
        for (char i = 'a'; i < 'z'; i++) {
            System.out.println(i);
        }
        // w inny sposób
        for (int i = 97; i < 123; i++) {
            System.out.println((char) i);
        }
        // wypisanie od 'a' do 'z'
        for (char i = 'a'; i < 'z'; i++) {
            if (i % 5 == 0) {
                System.out.println(i);
            }
        }

    }
}
