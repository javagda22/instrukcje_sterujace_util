package com.javagda22.instrukcjesterujące.instrukcjaFor;

public class InstrukcjaFor {
    public static void main(String[] args) {

        int warunek = 5;
        // int licznik = 0; - inicjalizacja - wykonuje się 1 raz
        // licznik < warunek; - warunek - przed (każdym) obiegiem
        // licznik++; - inkrementacja / aktualizacja - PO OBIGEU!
        int licznik;
        for (licznik = 1; licznik <= warunek; licznik++) {
            System.out.println(licznik);
        }
        // licznik poniżej do inna zmienna (kolejna, drugi raz
        // zadeklarowana
        for (licznik = 1; licznik <= warunek; licznik++) {
            System.out.println(licznik);
        }
        System.out.println(licznik);
    }
}
