package com.javagda22.instrukcjesterujące;

public class DemoSwitch {
    public static void main(String[] args) {
        int zmienna = 2;

        switch (zmienna) {
            case 1:
                System.out.println("Case 1");
                break;
            case 2:
                System.out.println("Case 2");
                break;
            case 3:
                System.out.println("Case 3");
                break;
            default:
                System.out.println("Default");
        }

    }
}
